#!/bin/bash

links_telegram="links_telegram.txt"
links_spotify="links_spotify.txt"

while true
do
	date
	python3 ./get_messages.py 
	list=$(jq -r '.[] | .message' < all_messages.json | grep -o -P 'https://open.spotify.com/track/.{0,22}' | uniq )

	for i in $list; do echo $i | tail -c 23 ;done > $links_telegram

	python3 ./get_tracks.py > $links_spotify
	
	if [ -s "$links_telegram" ] && [ -s "$links_spotify" ]; then

		links="$(comm -23 <(sort "$links_telegram") <(sort "$links_spotify") | uniq | head -n1)"

		if [ -n "${links}" ]
		then
				for link in $links
				do 
				(python3 add_track.py $link && echo $link added correctly) || (echo There was a problem with add $link to playlist && exit 1)
				done
		else 
				echo "Nothing to update"
		fi

		rm -f all_messages.json $links_telegram $links_spotify
	else
		echo "Somethings gone wrong"
	fi
	sleep 3600
done

