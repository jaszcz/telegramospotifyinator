# Import libs
import configparser
import spotipy
import os
from spotipy.oauth2 import SpotifyClientCredentials

# Reading Configs
config = configparser.ConfigParser()
config.read("config.ini")

# Assign variables

client_id = config['spotify']['client_id']
client_secret = config['spotify']['client_secret']
redirect_uri = config['spotify']['redirect_uri']
playlist_id = config['spotify']['playlist_id']

sp = spotipy.Spotify(client_credentials_manager=SpotifyClientCredentials(client_id=client_id,
                                                                         client_secret=client_secret
                                                                         ))
offset = 0


while True:
    response = sp.playlist_items(playlist_id,
                                    offset=offset,
                                    fields='items.track.id',
                                    additional_types=['track'])
    if len(response['items']) == 0:
        break

    for x in range(len(response['items'])) :
        r = response['items']
        item = r[x]
        print(item['track']['id'])
    offset = offset + len(response['items'])