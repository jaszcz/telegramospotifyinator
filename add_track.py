# Import libs
import configparser
import os
import sys
import spotipy
from spotipy.oauth2 import SpotifyOAuth


# Reading Configs
config = configparser.ConfigParser()
config.read("config.ini")

# Assign variables
client_id = config['spotify']['client_id']
client_secret = config['spotify']['client_secret']
redirect_uri = config['spotify']['redirect_uri']
playlist_id = config['spotify']['playlist_id']

sp = spotipy.Spotify(auth_manager=SpotifyOAuth(client_id=client_id,
                                               client_secret=client_secret,
                                               redirect_uri=redirect_uri,
                                               scope="playlist-modify-private", 
                                               cache_path='.cache'
                                               ))

sp.playlist_add_items(playlist_id, [sys.argv[1]])
